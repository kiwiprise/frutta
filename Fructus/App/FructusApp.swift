//
//  FructusApp.swift
//  Frutta
//
//  Created by Umberto Casa on 26/06/21.
//
import SwiftUI
@main
struct FructusApp: App {
    @AppStorage("isOnboarding") var  isOnboarding: Bool = true
    
    
    var body: some Scene {
        WindowGroup {
            if isOnboarding{
                OnboardingView()
            }else{
                ContentView()
            }
            
        }
    }
}
